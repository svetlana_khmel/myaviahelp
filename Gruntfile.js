'use strict';

module.exports = function(grunt) {
    //Load all grunt tasks
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-sw-precache');
    grunt.loadNpmTasks('grunt-sw-precache');
    grunt.loadNpmTasks('grunt-babel');

    //require('load-grunt-tasks')(grunt);  //Now we can load all tasks this way (npm module load-grunt-tasks)

    grunt.initConfig({
        watch: {
            js: {
                files: ['public/js/*.js'],
                tasks: ['browserify' ]
            },
            css: {
                files: "public/less/*.less",
                tasks: ["less"]
            }
        },
        browserify: {
            js: {
                src: 'public/js/*.js',
                dest: 'public/build/bundle.js'
            },
            css: {
                files: "public/less/*.less",
                tasks: ["less"]
            }
        },
        less: {
            // production config is also available
            development: {
                options: {
                    // Specifies directories to scan for @import directives when parsing.
                    // Default value is the directory of the source, which is probably what you want.
                    paths: ["public/stylesheets/"]
                },
                files: {
                    // compilation.css  :  source.less
                    "public/build/css/app.css": "public/less/app.less"
                }
            }
        },
        copy: {// THIS TASK DOESN'T WORK!!!
            files: {
                cwd: 'node_modules/font-awesome/fonts',  // set working folder / root to copy
                src: '**/*',           // copy all files and subfolders
                dest: 'public/build/fonts',    // destination folder
                expand: true           // required when using cwd
            }
        },
        'sw-precache': {
            options: {
                baseDir: 'public/build/',
                cacheId: 'progressive-test-package',
                workerFileName: 'sw.js',
                verbose: true
            },
            'default': {
                staticFileGlobs: [
                    'public/css/**/*.css',
                    'public/*.html',
                    'font/**/*.{woff,ttf,svg,eot}',
                    'public/images/**/*.{gif,png,jpg}',
                    'public/build/*.js'
                ]
            },
            'develop': {
                staticFileGlobs: [
                    'font/**/*.{woff,ttf,svg,eot}'
                ]
            }
        },
        concat_css: {
            options: {
                // Task-specific options go here.
            },
            all: {
                src: ["public/css/*.css"],
                dest: "public/build/css/app.css"
            }
        },
        babel: {
            options: {
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            jsx: {
                files: [{
                    expand: true,
                    cwd: 'public/admin/js', // Custom folder
                    src: ['*.jsx'],
                    dest: 'public/js/', // Custom folder
                    ext: '.js'
                }]
            }
        }

    });

    //the default task running 'grunt' in console is 'watch'
    grunt.registerTask('default', ['watch', 'babel']);
    grunt.registerTask('copy', ['copy', 'browserify','sw-precache']);
};