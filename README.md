# README #

The goal of this project:

* To create progressive application, mean it should be lightweight, data based site, with serviceWorker for working offline, 
responsive, with manifest.json for mobile devices.

### Technologies in use:

*** Node, Express, ejs template engine on backend, creation/reading json on backend, routing, http-auth for authification

*** Handlebars, JQuery, Commonjs pattern

*** Parallax (rellax lib)

*** Less for styles

*** Grunt to build the project

*** Bootstrap 3 elements (Navigation, Scrollspy, Carouser);


### What is this repository for? ###

*** Site based on data (json)

*** Handlebars as template engine

*** serviceWorker for working offline

*** Bootstrap 3 elements (Navigation, Scrollspy, Carouser)

*** Site is responsive

*** Localization with i18n (3 languages)

*** Send email from the site


*** What was implemented?

*** Internationalization (i18n)



### How do I get set up? ###

1) Install dependencies:

* npm install 

2) Run server

* node app-server.js

2) Watch less and js changes:

* grunt watch

### Articles has been used: ###

https://github.com/l20n/l20n.js/blob/v3.x/docs/html.md

http://l20n.org/learn/

** To add new translation need to add data-i18n="translate" attribute and add key/value to en.json, ru.json, uk.json;

http://www.ab-weblog.com/en/internationalization-how-to-localize-html5-projects/

https://php.quicoto.com/use-babel-to-compile-react-jsx-with-grunt/

*** React.js : Server side rendering:

http://crypt.codemancers.com/posts/2016-09-16-react-server-side-rendering/

Great parallax library:

https://dixonandmoe.com/rellax/

https://github.com/senchalabs/connect#middlew
http://ejs.co/


******* Send email via JS
https://www.youtube.com/watch?v=zrXOjWICmGw
https://app.sendgrid.com/login

** Handy articles **

https://css-tricks.com/snippets/css/a-guide-to-flexbox/

