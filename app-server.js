var express = require('express');
var app = express();

var auth = require('http-auth');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');

//Configure basic auth;

var basic = auth.basic({
        realm: 'SUPER SECRET STUFF'
    },
    function (username, password, callback) {
        callback(username == 'admin' && password == 'password');
    }
);

// Create middleware that can be used to protect routes with basic auth
var authMiddleware = auth.connect(basic);


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
    response.render('public/index');
});

app.use(bodyParser.urlencoded({extended: false}));
process.env.NODE_ENV = 'production';

app.get('/admin',  authMiddleware, function (req, res) {
    var en;
    var ru;
    var uk;

    fs.readFile(path.join(__dirname + '/public/lang/en.json'), 'utf8', function (err, en) {
        en = JSON.parse(en);

        fs.readFile(path.join(__dirname + '/public/lang/ru.json'), 'utf8', function (err, ru) {
            ru = JSON.parse(ru);

            fs.readFile(path.join(__dirname + '/public/lang/uk.json'), 'utf8', function (err, uk) {
                uk = JSON.parse(uk);

                res.render('admin', { endata: en, rudata: ru, ukdata: uk });

                if (err) throw err;

            });

            if (err) throw err;

        });

        if (err) throw err;

    });

});

app.post('/getjson', function (req, res) {
    var data = req.body;
    var data_obj =  Object.keys(data)[0];
    var obj = JSON.parse(data_obj);

    var en_json = JSON.stringify(obj[0]);
    var ru_json = JSON.stringify(obj[1]);
    var uk_json = JSON.stringify(obj[2]);

    saveJson('en', en_json);
    saveJson('ru', ru_json);
    saveJson('uk', uk_json);
});

function saveJson (name, data) {

    fs.writeFile(path.join(__dirname + '/public/lang/'+ name +'.json'), data, function(err){
        if (err) {
            return console.log(err);
        }
        console.log(name +' json has been saved.');
    });
}

app.post('/post', function (req, res) {
    // using SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs

    var dataToSend =  'Name: ' + req.body.username + ' Email: ' + req.body.email + ' Phone: ' + req.body.phone;

    var helper = require('sendgrid').mail;
    var fromEmail = new helper.Email('test@example.com');
    var toEmail = new helper.Email('l0lka@ukr.net');
    var subject = "New application.";
    var content = new helper.Content('text/plain', dataToSend);
    var mail = new helper.Mail(fromEmail, subject, toEmail, content);

    var sg = require('sendgrid')('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');
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, function (error, response) {
        if (error) {
            console.log('Error response received');
        }
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
        res.send('Data has been sent successfully.')
    });
});

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});