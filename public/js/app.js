var $ = jQuery = require('jquery');
window.$ = $;

var Handlebars = require('handlebars');

require('bootstrap');

var Rellax = require('./relax');
var Validation = require('./validation');

$(function() {
    //window.Stellar = foo(jQuery, this, document);

    var rellax = new Rellax('.rellax');
    var topoffset = 50;

    // if ('serviceWorker' in navigator) {
    //     navigator.serviceWorker
    //         .register('../../build/sw.js')
    //         .then(function(){
    //             console.log('Service Worker Active');
    //         })
    // }


    var mailTemplate = $('#mail-template').html();
    var mailScript = Handlebars.compile(mailTemplate);

    $('.applymodal .modal-body').html(mailScript);

    $.getJSON('../data/coffee.json', function(data){
        $('.loader').fadeOut(1000);
        var slideshowTemplate = $('#slideshow-template').html();
        var slideshowScript = Handlebars.compile(slideshowTemplate);
        
        //var adoptionTemplate = $('#specials-template').html();
       // var adoptionScript = Handlebars.compile(adoptionTemplate);

       // var appointmentTemplate = $('#appointments-template').html();
       // var appointmentScript = Handlebars.compile(appointmentTemplate);

        $('#slideshow-content').append(slideshowScript(data));
       // $('#specials-content').append(adoptionScript(data));
       // $('#appointments-content').append(appointmentScript(data));

        //Replace IMG inside carousels with background image

        $('#slideshow .item img').each(function () {
            var imgSrc = $(this).attr('src');
            $(this).parent().css({'background-image': 'url(' + imgSrc + ')'});
            $(this).remove();
        });

        //Activate carousel
        $('.carousel').carousel({
            pause: false
        })
    });

    $(document).on('click', '.openmodal', function () {
        var title = $(this).find('.title').html();
        var content = $(this).find('.content').html();

        $('.wisdommodal .modal-title').html(title);
        $('.wisdommodal .modal-body').html(content);

        // $('.modal-coffeebreed').html($(this).data('coffeebreed'));
        // $('.modal-coffeeowner').html($(this).data('coffeeowner'));
        // $('.modal-coffeeinfo').html($(this).data('coffeeinfo'));
        // $('.modal-coffeeimage').attr('src', 'images/menu/' + $(this).data('coffeeimage') + '.jpg');
        // $('.modal-coffeeimage').attr('alt', $(this).data('coffeeimage') + ' photo');

    });

    $(document).on('click', '.btn-more', function (e) {
        e.preventDefault();
        $(this).next().toggle();
    });

    $(document).on('click', '.to-top', function () {
        console.log("to top");
        $('body,html').animate({
            scrollTop: 0
        }, 400);
    });

    // $(document).on('click', '.modal-body .btn-open-form',  function () {
    //     var form = $('.mail-form').html();
    //     console.log(form);
    //     console.log(this);
    //     $(this)
    //         .append(form)
    //         .remove();
    //
    // });

    $('.navbar-fixed-top').on('activate.bs.scrollspy', function () {
        var hash = $(this).find('li.active a').attr('href');

        if (hash != '#slideshow') {
            $('header nav').addClass('inbody');
        } else {
            $('header nav').removeClass('inbody');
        }
    });

    //Use smooth scrolling when clicking on navigation
    $('.navbar a').click(function() {
        if (location.pathname.replace(/^\//,'') ===
            this.pathname.replace(/^\//,'') &&
            location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top-topoffset+2
                }, 500);
                return false;
            } //target.length
        } //click function
    }); //smooth scrolling

    $(document).on('click', '.submit-form', function(e){
        e.preventDefault();
        var form = $('.applymodal .mail-form form');
        var modalBody = $('.applymodal .modal-body');
        var data = $(form).serializeArray();
        var dfd = $.Deferred();
        var mailDfd = $.Deferred();

        dfd
            .done([Validation(data, dfd)])
            .done(function(res){
                console.log('resp ', res);

                mailDfd
                    .done([showFormTooltips(form, data, res, mailDfd)])
                    .done(function(resp) {
                        $('.applymodal .cover').html('<div class="resp">'+ resp + '</div>');
                    });
                modalBody.append('<div class="cover"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>');
            });
    });

    $('.applymodal').on('hidden.bs.modal', function () {
        $('.applymodal .cover').remove();
    });

    $('body').scrollspy({
        target: 'header .navbar',
        offset: topoffset
    });

    require('./translation_keys');
});

function showFormTooltips (form, data, res, mailDfd) {

    var passed = [];

    for (var key in res) {
        var tooltip = $(form).find("[data-tooltip='" + key + "']");

        if (res[key] !== 'passed') {
            $(tooltip).html(res[key]);
        } else {
            $(tooltip).html("");
            passed.push(res[key]);
        }
    }

    if (passed.length == Object.keys(res).length) {

        $.ajax({
            type: "POST",
            url: '/post',
            data: data,
            success: function (res) {
                if(typeof res === "string") {
                    mailDfd.resolve(res);
                }
            },
            dataType: 'text'
        });
    }
}