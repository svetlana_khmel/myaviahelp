var $ = jQuery = require('jquery');
var i18n = require('./lib/i18n');

(function ($, i18n) {

    // document.addEventListener("load", function(event){
    //     loaded ();
    // });

    $.getJSON ('../lang/en.json', function (data) {
        console.log(data);
    });

    function loaded () {
        console.log('Loaded');
        var lang = getParameterValue("lang");
        if (lang != "") String.locale = lang;

        document.title = _(document.title);

        //localizeHTMLTag("[data-i18n='headertext']");
        localizeHTMLTag();

    }

    /* Some helper functions: */

    var _ = function (string) {
        return string.toLocaleString();
    };

    function localizeHTMLTag()
    {
        var elm = document.querySelectorAll("[data-i18n='translate']");

        elm.forEach(function(val, index){
            console.log("____ value ", val ,"  ..index ", index);
            val.innerHTML = _(val.innerHTML);
        });
    }

    function getParameterValue(parameter)
    {
        parameter = parameter.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + parameter + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if(results == null)
            return "";
        else
            return results[1];
    }
    loaded();
    /* End localization: */
})($, i18n);